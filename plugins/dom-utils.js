
export default (context, inject) => {
    const plainAttributes = [
        'for',
        'inputmode',
        'minlength',
        'maxlength',
        'min',
        'max',
        'pattern'
    ]

    const createElement = (tag, attrs) => {
        const el = document.createElement(tag)
        plainAttributes.forEach((plainAttr) => {
            if (attrs && plainAttr in attrs && attrs[plainAttr]) {
                el.setAttribute(plainAttr, attrs[plainAttr])
            }
            if (attrs) {
                delete attrs[plainAttr]
            }
        })
        Object.assign(el, attrs)
        return el
    }


    const downloadBlob = (blob, fileName) => {
        const link = createElement('a')
        const url = URL.createObjectURL(blob)
        link.href = url
        link.download = fileName
        document.body.appendChild(link)
        link.click()
    }

    inject('downloadBlob', downloadBlob);
    context.$downloadBlob = downloadBlob
}