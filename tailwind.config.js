/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
    purge: {
      enabled: process.env.NODE_ENV === "production",
      options: {
        
      }
    },
    theme: {
      extend: {
        
      }
    },
    variants: {
      
    },
    //plugins: [require("@tailwindcss/ui")]
  };
  